from sklearn import svm, datasets

class klasyfikator:
	def __init__(self, X, y) : 
		# we create an instance of SVM and fit out data. We do not scale our
		# data since we want to plot the support vectors
		C = 1.0  # SVM regularization parameter
		self.svc = svm.SVC(kernel='linear', C=C).fit(X, y)
		self.rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(X, y)
		self.poly_svc = svm.SVC(kernel='poly', degree=3, C=C).fit(X, y)
		self.lin_svc = svm.LinearSVC(C=C).fit(X, y)