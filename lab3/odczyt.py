import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets

class odczyt:
	def __init__ (self) : 
		iris = datasets.load_iris()
		self.X = iris.data[:, :2]	# we only take the first two features. We could
                # avoid this ugly slicing by using a two-dim dataset
		self.y = iris.target
		self.h = .02  # step size in the mesh
